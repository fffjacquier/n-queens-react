const path = require('path')
// const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const PATHS = {
  src: path.resolve(__dirname, 'src'),
  build: path.resolve(__dirname, 'public')
}

module.exports = {
  entry: `${PATHS.src}/index.js`,
  output: {
    path: PATHS.build,
    filename: 'bundle.js'
    // publicPath: '/'
  },
  /* plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html'),
      inject: 'body',
      filename: 'index.html'
    })
  ],
  devServer: {
    contentBase: PATHS.build,
    inline: true,
    port: 4000
  },
  devtool: 'eval-source-map', */
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  }
}
