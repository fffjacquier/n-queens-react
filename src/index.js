import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Board from './components/Board'

class App extends Component {
  constructor (props) {
    super(props)
    this.arr = []
    for (let i = 0; i < this.props.size; i++) {
      this.arr.push(i)
    }
    this.state = {
      clickedCells: []
    }
    this.updateCell = this.updateCell.bind(this)
  }

  updateCell (line, index, clicked) {
    const clickedCells = [...this.state.clickedCells]
    const clickedCell = {line: line, col: index}
    const cellIndex = clickedCells.findIndex((cell) => cell && cell.line === line && cell.col === index)

    if (cellIndex === -1) {
      clickedCells.push(clickedCell)
      this.checkConflict(clickedCells)
      this.setState({ clickedCells })
    } else {
      // remove from array if unclicked
      if (!clicked) {
        clickedCells.splice(cellIndex, 1)
        this.checkConflict(clickedCells, true)
        this.setState({ clickedCells })
      }
    }
  }

  checkConflict (cellsArr, resetBool = false) {
    let i = 0
    if (resetBool) {
      for (i; i < cellsArr.length - 1; i++) {
        for (let j = i + 1; j < cellsArr.length; j++) {
          cellsArr[i].conflict = false
          cellsArr[j].conflict = false
        }
      }
    }
    for (i = 0; i < cellsArr.length - 1; i++) {
      for (let j = i + 1; j < cellsArr.length; j++) {
        let conflict = this.inConflict(cellsArr[i], cellsArr[j])
        if (!cellsArr[i].conflict && conflict) {
          cellsArr[i].conflict = true
        }
        if (!cellsArr[j].conflict && conflict) {
          cellsArr[j].conflict = true
        }
      }
    }
  }

  inConflict (el1, el2) {
    return el1.line === el2.line || el1.col === el2.col || Math.abs(el1.line - el2.line) === Math.abs(el1.col - el2.col)
  }

  render () {
    return (
      <div>
        <h1>Board</h1>
        <Board arr={this.arr} clickedCells={this.state.clickedCells} updateCell={this.updateCell} />
      </div>
    )
  }
}

ReactDOM.render(<App size='8' />, document.getElementById('react'))
