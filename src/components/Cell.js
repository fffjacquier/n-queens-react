import React, { Component } from 'react'

class Cell extends Component {
  constructor (props) {
    super(props)
    this.state = {
      clicked: false
    }
  }
  debugCell () {
    return `${this.props.lineNumber}, ${this.props.index}`
    // = ${this.props.lineNumber + this.props.index}
  }
  renderEmptyCell () {
    return (
      <td onClick={e => this.handleClick(e)} className='cell'>{this.debugCell()}</td>
    )
  }
  renderQueenCell () {
    return (
      <td onClick={e => this.handleClick(e)} className='cell clicked'>{this.debugCell()}</td>
    )
  }
  renderConflictCell () {
    return (
      <td onClick={e => this.handleClick(e)} className='cell clicked conflict'>{this.debugCell()}</td>
    )
  }

  handleClick (e) {
    this.state.clicked = !this.state.clicked
    this.props.updateCell(this.props.lineNumber, this.props.index, this.state.clicked)
  }
  render () {
    if (this.state.clicked) {
      if (this.props.clickedCells) {
        const cellData = this.props.clickedCells.filter((el) => el.line === this.props.lineNumber && el.col === this.props.index)[0] || null
        if (cellData && cellData.conflict) {
          return this.renderConflictCell()
        }
      }
      return this.renderQueenCell()
    }
    return this.renderEmptyCell()
  }
}

export default Cell
