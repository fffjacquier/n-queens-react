import React, { Component } from 'react'
import Line from './Line'

class Board extends Component {
  renderLines (lineNumber, arr) {
    return <Line key={lineNumber} arr={arr} lineNumber={lineNumber} updateCell={this.props.updateCell} clickedCells={this.props.clickedCells} />
  }
  render () {
    return (
      <table>
        <tbody>
          {this.props.arr.map((item, index, arr) => this.renderLines(item, arr))}
        </tbody>
      </table>
    )
  }
}

export default Board
