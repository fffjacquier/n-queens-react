import React, { Component } from 'react'
import Cell from './Cell'

class Line extends Component {
  render () {
    return <tr>
      {this.props.arr.map(index => <Cell key={index} index={index} lineNumber={this.props.lineNumber} updateCell={this.props.updateCell} clickedCells={this.props.clickedCells} />)}
    </tr>
  }
}

export default Line
